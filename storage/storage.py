import abc
import logging
from pathlib import Path

from storage.entities import Entity, Results, Submission, TestFiles, UploadedEntity
from storage.utils import copy_and_overwrite, unique_name

log = logging.getLogger(__name__)


class AbstractStoragePart(object):
    def __init__(self, storage: 'Storage'):
        """Creates instance of the Abstract storage Factory
        Args:
            storage(Storage):
        """
        self.storage = storage

    @property
    @abc.abstractmethod
    def _entity_klass(self):
        """Returns the entity class

        Returns(Class): Entity class
        """
        return Submission

    @property
    def path(self) -> Path:
        """Path to the base directory

        Returns(Path): Instance of the Path to the base dir
        """
        return Path()

    @property
    def config(self) -> dict:
        """Gets an config

        Returns(dict): Gets an config
        """
        return self.storage.config

    @property
    def workspace_path(self) -> Path:
        """Workspace path

        Returns (Path): Workspace path
        """
        return Path(self.config.get('workspace_dir'))

    def create(self, entity_id: str, **kwargs):
        """Creates an instance of the entity

        Args:
            entity_id(str): UUID of the entity
            **kwargs:

        Returns(Entity):
        """
        log.info(f"[CREATE] {self._entity_klass.__name__} [{entity_id}]: {kwargs}")
        upload = self.create_upload(entity_id, **kwargs)
        return upload.process()

    def create_upload(self, entity_id, **kwargs):
        log.info(f"[UPLOAD] {self._entity_klass.__name__} [{entity_id}]: {kwargs}")
        entity = self.__instance(entity_id=entity_id)
        upload = self.__wrap_entity(entity=entity, **kwargs)
        return upload

    def get(self, entity_id: str):
        """Gets an instance of the entity

        Args:
            entity_id(str): Id of the entity

        Returns(Entity):

        """
        return self.__instance(entity_id=entity_id)

    def clone(self, from_id: str, to_id: str) -> Entity:
        log.info(f"[CLONE] {self._entity_klass.__name__}: {from_id} -> {to_id}")
        from_instance = self.get(from_id)
        to_instance = self.__instance(entity_id=to_id)
        copy_and_overwrite(
            src=from_instance.subdir('files'),
            dst=to_instance.subdir('files')
        )
        return to_instance

    def __instance(self, entity_id) -> Entity:
        log.debug(f"[GET] {self._entity_klass.__name__}: #{entity_id}")
        return self._entity_klass(entity_id=entity_id, base_dir=self.path)

    def __wrap_entity(self, entity, **kwargs) -> UploadedEntity:
        uname = unique_name(entity.entity_id)
        workspace = self.workspace_path / uname
        return UploadedEntity(entity=entity,
                              workspace=workspace,
                              **kwargs)


class SubmissionStorage(AbstractStoragePart):
    @property
    def _entity_klass(self):
        return Submission

    @property
    def path(self) -> Path:
        """Returns the instance of the submission files
        Returns(Path): test path files
        """
        return Path(self.config.get('submissions_dir'))


class TestFilesStorage(AbstractStoragePart):
    @property
    def _entity_klass(self):
        return TestFiles

    @property
    def path(self) -> Path:
        """Returns the instance of the path of the test files
        Returns(Path): test path files
        """
        return Path(self.config.get('test_files_dir'))

    def update(self, entity_id: str, **kwargs):
        log.info(f"[UPDATE] {self._entity_klass.__name__} [{entity_id}]: {kwargs}")
        test_files = self.get(entity_id=entity_id)
        if test_files:
            test_files.delete()
        return self.create(entity_id=entity_id, **kwargs)


class ResultsStorage(AbstractStoragePart):
    @property
    def _entity_klass(self):
        return Results

    @property
    def path(self) -> Path:
        """Returns the path to the results
        Returns(Path): results path
        """
        return Path(self.config.get('results_dir'))


class Storage:
    def init_storage(self, **config):
        """Initializes the storage with configuration
        Args:
            config(dict): Storage config
        Configuration:
            test_files_dir: Directory where the test files should be stored
            submissions_dir: Directory where submission files should be stored
            results_dir: Directory where submission results should be stored
            workspace_dir: Temporary workspace
        """
        self._config = config

    def __init__(self, **config):
        """Creates an instance of the storage

        Instance contains the configuration

        Args:
            test_files_dir(str, Path): Directory where the test files should be stored
            submissions_dir(str, Path): Directory where submission files should be stored
            workspace_dir(str, Path): Temporary workspace
        Configuration:

        """
        self._config = config
        self.__submissions = SubmissionStorage(self)
        self.__test_files = TestFilesStorage(self)
        self.__results = ResultsStorage(self)

    @property
    def config(self) -> dict:
        """Gets a instance of the configuration

        Returns(dict): The configuration instance
        """
        return self._config

    @property
    def submissions(self) -> SubmissionStorage:
        """Returns the submissions factory instance

        Returns(SubmissionStorage): Submissions factory instance
        """
        return self.__submissions

    @property
    def test_files(self) -> TestFilesStorage:
        """Returns the test files factory instance

        Returns(TestFilesStorage): Tests files factory instance
        """
        return self.__test_files

    @property
    def results(self) -> ResultsStorage:
        """Returns the results factory instance
        Returns(ResultsStorage): Results storage factory instance

        """
        return self.__results
