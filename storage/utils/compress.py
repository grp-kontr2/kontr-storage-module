import logging
from pathlib import Path

from storage.utils import zipdir, unzipdir

log = logging.getLogger(__name__)


def compress_entity(entity) -> Path:
    """Compressing the entity to the zip file
    Arguments:
        entity(Entity) Entity instance
    Returns(Path): Path to the zip file
    """
    log.info(f"[ZIP] Compressing {entity.entity_id} to file: {entity.zip_path}")
    zipdir(entity.zip_path, entity.path)
    return entity.zip_path


def decompress_entity(entity) -> Path:
    log.info(f"[ZIP] Decompressing {entity.entity_id} to dir: {entity.path}")
    unzipdir(entity.zip_path, entity.path)
    return entity.path
