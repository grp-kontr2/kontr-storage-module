import re
import string
import secrets

NOT_ALLOWED_FOR_NAME = string.punctuation.replace('_', '')


def remove_punctuation(s: str):
    table = s.maketrans('', '', NOT_ALLOWED_FOR_NAME)
    return s.translate(table)


def substitute_spaces(s, sub='_'):
    return sub.join(s.split())


def convert_to_snake(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def normalize_name(name):
    if name is None:
        return name
    name = convert_to_snake(name)
    name = name.lower()
    name = remove_punctuation(name)
    name = substitute_spaces(name)
    return name


def unique_name(name):
    return f"{name}_{secrets.token_urlsafe(8)}"
