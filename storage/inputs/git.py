import logging
from git import Repo

log = logging.getLogger(__name__)


def git_clone(entity):
    """Clones the repository
    Args:
        entity: Entity instance
    Returns(Entity): Entity instance
    """
    return GitWrapper(entity=entity).clone()


class GitEntityConfig(object):
    def __init__(self, entity):
        self.entity = entity

    @property
    def config(self) -> dict:
        """Entity config
        Returns(dict): Entity config
        """
        return self.entity.config

    @property
    def url(self) -> str:
        """Git repo url
        Returns(str): Repository url
        """
        return self.config['url']

    @property
    def branch(self) -> str:
        """Branch name
        Returns(str): Branch name
        """
        return self.config.get('branch')

    @property
    def checkout(self) -> str:
        """Checkout (commit, branch, tag)
        Returns(std): Which version to checkout
        """
        return self.config.get('checkout')

    @property
    def effective_checkout(self) -> str:
        """Effective checkout - whether to checkout branch or tag or commit
        Returns(str): Checkout name
        """
        return self.checkout or self.branch


class GitWrapper(object):
    def __init__(self, entity):
        self.entity = entity
        self.config = GitEntityConfig(self.entity)
        self._repo = None

    @property
    def repo(self) -> Repo:
        """Gets an instance of the GitPython Repo object
        Returns(Repo): Instance of the GitPython Repo
        """
        return self._repo

    def clone(self) -> Repo:
        """Clones the repository
        Returns:

        """
        log.info(f"[GIT] Cloning from: {self.config.url} -> {self.entity.workspace}")
        params = dict(
            url=self.config.url,
            to_path=self.entity.workspace
        )
        if self.config.branch:
            params['branch'] = self.config.branch
        cloned_repo = Repo.clone_from(**params)
        digest = cloned_repo.head.commit.hexsha
        self.entity.version = digest
        self.entity.additional = cloned_repo.head
        log.debug(f"[GIT] Cloned {digest}: {cloned_repo}")
        self._repo = cloned_repo
        return cloned_repo
