from setuptools import setup, find_packages

setup(name='kontr-storage',
      version='1.0',
      description='Kontr storage module',
      author='Peter Stanko',
      author_email='stanko@mail.muni.cz',
      url='https://gitlab.fi.muni.cz/grp-kontr2/storage',
      packages=find_packages(exclude=("tests",)),
      include_package_data=True,
      install_requires=[
          'gitpython'
          ],
      extras_require={
          'dev': [
              'pytest',
              'coverage',
              'pytest-mock',
              ],
          'docs': [
              'sphinx',
              ]
          }
      )
