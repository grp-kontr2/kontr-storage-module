from pathlib import Path
import pytest

from storage.utils import zipdir, unique_name, unzipdir


def tmp_file(src, i, ext):
    file = src.join(f'test-{i}.{ext}')
    file.write(f'test-{i}.{ext}')


def tmp_dir(work, name):
    src = work.mkdir(name)
    for i in range(10):
        tmp_file(src, i, 'c')
        tmp_file(src, i, 'txt')


@pytest.fixture
def workspace(tmpdir):
    work = tmpdir.mkdir('work')
    tmp_dir(work=work, name='src')
    tmp_dir(work=work, name='data')
    work.mkdir('noot')
    return Path(work)


@pytest.fixture
def zip_file(workspace, tmpdir):
    fname = Path(tmpdir) / 'zip_file.zip'
    zipdir(fname, workspace)
    return fname


@pytest.fixture()
def output_dir(zip_file, tmpdir):
    out_dir = tmpdir.mkdir('out')
    unzipdir(source_zip_file=zip_file, destination_directory=out_dir)
    return Path(out_dir)


def test_zipdir(zip_file: Path):
    assert zip_file
    assert zip_file.exists()
    assert zip_file.stat().st_size > 0


def test_unzipdir(output_dir: Path):
    assert output_dir.exists()
    files = list(output_dir.glob('*'))
    assert len(files)
