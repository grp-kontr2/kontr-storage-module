import logging.config
from pathlib import Path

import pytest

from storage import Storage

TESTS_PATH = Path(__file__).parent
RESOURCES_PATH = TESTS_PATH / 'resources'

LOGGER_CONFIG = dict(
    version=1,
    disable_existing_loggers=True,
    formatters={
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
    handlers={
        'default': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
            },
        },
    loggers={
        'storage': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
            },
        'git': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
            },
        'tests': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
            }
        })

logging.config.dictConfig(LOGGER_CONFIG)


@pytest.fixture
def log():
    return logging.getLogger(__name__)


@pytest.fixture
def workspace_dir(tmpdir):
    return tmpdir.mkdir('workspace')


@pytest.fixture
def test_files_dir(tmpdir):
    return tmpdir.mkdir('test_files')


@pytest.fixture()
def submissions_dir(tmpdir):
    return tmpdir.mkdir('submissions')


@pytest.fixture()
def results_dir(tmpdir):
    return tmpdir.mkdir('results')


@pytest.fixture
def storage_instance(test_files_dir, workspace_dir, submissions_dir, results_dir) -> Storage:
    return Storage(test_files_dir=test_files_dir,
                   workspace_dir=workspace_dir,
                   submissions_dir=submissions_dir,
                   results_dir=results_dir)
